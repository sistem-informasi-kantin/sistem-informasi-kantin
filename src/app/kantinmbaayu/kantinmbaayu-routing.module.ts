import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { KantinmbaayuPage } from './kantinmbaayu.page';

const routes: Routes = [
  {
    path: '',
    component: KantinmbaayuPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class KantinmbaayuPageRoutingModule {}
