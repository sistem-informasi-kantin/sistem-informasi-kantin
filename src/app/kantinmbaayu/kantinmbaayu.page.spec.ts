import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { KantinmbaayuPage } from './kantinmbaayu.page';

describe('KantinmbaayuPage', () => {
  let component: KantinmbaayuPage;
  let fixture: ComponentFixture<KantinmbaayuPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KantinmbaayuPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(KantinmbaayuPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
