import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { KantinmbaayuPageRoutingModule } from './kantinmbaayu-routing.module';

import { KantinmbaayuPage } from './kantinmbaayu.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    KantinmbaayuPageRoutingModule
  ],
  declarations: [KantinmbaayuPage]
})
export class KantinmbaayuPageModule {}
