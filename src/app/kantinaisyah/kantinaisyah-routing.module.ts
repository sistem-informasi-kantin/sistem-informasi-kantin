import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { KantinaisyahPage } from './kantinaisyah.page';

const routes: Routes = [
  {
    path: '',
    component: KantinaisyahPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class KantinaisyahPageRoutingModule {}
