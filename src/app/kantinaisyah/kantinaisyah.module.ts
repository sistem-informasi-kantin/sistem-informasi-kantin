import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { KantinaisyahPageRoutingModule } from './kantinaisyah-routing.module';

import { KantinaisyahPage } from './kantinaisyah.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    KantinaisyahPageRoutingModule
  ],
  declarations: [KantinaisyahPage]
})
export class KantinaisyahPageModule {}
