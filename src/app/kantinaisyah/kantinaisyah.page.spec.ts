import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { KantinaisyahPage } from './kantinaisyah.page';

describe('KantinaisyahPage', () => {
  let component: KantinaisyahPage;
  let fixture: ComponentFixture<KantinaisyahPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KantinaisyahPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(KantinaisyahPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
