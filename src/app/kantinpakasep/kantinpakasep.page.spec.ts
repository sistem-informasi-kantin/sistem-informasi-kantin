import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { KantinpakasepPage } from './kantinpakasep.page';

describe('KantinpakasepPage', () => {
  let component: KantinpakasepPage;
  let fixture: ComponentFixture<KantinpakasepPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KantinpakasepPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(KantinpakasepPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
