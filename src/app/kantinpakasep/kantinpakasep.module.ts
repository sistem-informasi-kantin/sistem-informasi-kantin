import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { KantinpakasepPageRoutingModule } from './kantinpakasep-routing.module';

import { KantinpakasepPage } from './kantinpakasep.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    KantinpakasepPageRoutingModule
  ],
  declarations: [KantinpakasepPage]
})
export class KantinpakasepPageModule {}
