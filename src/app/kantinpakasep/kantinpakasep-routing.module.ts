import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { KantinpakasepPage } from './kantinpakasep.page';

const routes: Routes = [
  {
    path: '',
    component: KantinpakasepPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class KantinpakasepPageRoutingModule {}
